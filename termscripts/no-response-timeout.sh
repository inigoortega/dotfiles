#!/usr/bin/env sh

usage() {
    printf "HELP ME"
}

while getopts ":t:r:f:" option; do
    case "${option}" in
        t)
            time="${OPTARG}"
            ;;
        r)
            time4rest="${OPTARG}"
            ;;
        f)
            time4first="${OPTARG}"
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

[ -z "$time" ] && time="30"
[ -z "$time4rest" ] && time4rest="$time"
[ -z "$time4first" ] && time4first="$time"

{
    sleep "$time4first"
    kill "$$" 2>/dev/null
} &
checkpoint_pid="$!"

fifo="/tmp/no-response-timout-fifo$(date +%s%N)"
mkfifo "$fifo"
sh -c "$*" > "$fifo" &
command_pid="$!"

cleanup() {
    kill "$command_pid" 2>/dev/null
    rm "$fifo"
}
trap cleanup TERM INT PIPE

while read -r line; do
    kill "$checkpoint_pid"
    {
        sleep "$time4rest"
        kill "$$" 2>/dev/null
    } &
    checkpoint_pid="$!"

    echo "$line"
done < "$fifo"

rm "$fifo"
kill "$checkpoint_pid" 2>/dev/null
