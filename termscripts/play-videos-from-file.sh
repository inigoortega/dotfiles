#!/usr/bin/env sh

[ -z "$1" -o ! -f "$1" ] && exit 1

length="$(wc -l "$1" | awk '{print $1}')"

current_video=0
while [ $length -ne $current_video ]
do
    current_video="$(( $current_video + 1 ))"

    video="$(sed "$current_video q;d" "$1")"

    $VIDEOPLAYER "$video"

    length="$(wc -l "$1" | awk '{print $1}')"
    # if [ "$length" -eq "$current_video" ]; then
    #     sleep 5m
    # fi
done
