#!/usr/bin/env bash

update_kernel () {
    command -v update_kernel &&

    emerge -v --update --deep --with-bdeps=y --newuse \
        sys-kernel/gentoo-sources &&

    next_number="$(($(eselect kernel list | wc -l) - 1))" &&

    echo "New kernel: $(eselect kernel list | tail -n 1)" &&

    eselect kernel set $next_number &&

    cp "$old_config" /usr/src/linux/.config &&

    cd /usr/src/linux &&

    make syncconfig &&
    make modules_prepare &&
    emerge -v @module-rebuild &&
    make -j2 &&
    make modules_install &&
    make install &&

    grub-mkconfig -o /grub/libreboot_grub.cfg
}

old_config="$KERNEL_CONFIGS/kernel-config-$(uname -r)" &&

cp /usr/src/linux/.config "$old_config" &&

export old_config
export -f update_kernel
su -c update_kernel
