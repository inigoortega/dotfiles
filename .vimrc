call plug#begin('~/.vim/plugged')

" For LaTeX
Plug 'vim-latex/vim-latex'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-commentary' " Commentary.vim
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes' " Airline Themes
Plug 'michaeljsmith/vim-indent-object'

" NerdTree
" Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
" Plug 'scrooloose/nerdtree'

" Plug 'Reewr/vim-monokai-phoenix'
" Plug 'rafi/awesome-vim-colorschemes'
" Plug 'terryma/vim-multiple-cursors'
Plug 'hdima/python-syntax' 
Plug 'mileszs/ack.vim'
Plug 'gregsexton/MatchTag'
Plug 'xolox/vim-misc'
Plug 'xolox/vim-easytags'
Plug 'jceb/vim-orgmode'
Plug 'joshdick/onedark.vim'
Plug 'gruvbox-community/gruvbox'
Plug 'vim-scripts/ReplaceWithRegister'
Plug 'christoomey/vim-system-copy'
" Plug 'python-mode/python-mode', { 'branch': 'develop' }
" Plug 'tikhomirov/vim-glsl'
" Plug 'vim-scripts/clips.vim'
Plug 'justinmk/vim-syntax-extra'
" " Pending tasks list
" Plug 'fisadev/FixedTaskList.vim' ", { 'for': 'python' }
" " " Python and other languages code checker
" Plug 'scrooloose/syntastic' , { 'for': 'python' }
" " Indent text object
" Plug 'michaeljsmith/vim-indent-object' , { 'for': 'python' }
" " Indentation based movements
" Plug 'jeetsukumaran/vim-indentwise' , { 'for': 'python' }
" " Python autocompletion, go to definition.
" Plug 'davidhalter/jedi-vim', { 'for': 'python' }
" " Better autocompletion
" Plug 'Shougo/neocomplcache.vim' , { 'for': 'python' }

" " Haskell prettifiers
" Plug 'jaspervdj/stylish-haskell'
" Plug 'nbouscal/vim-stylish-haskell'
Plug 'sheerun/vim-polyglot' , { 'for': 'haskell' }
" Plug 'chrisdone/hindent' , { 'for': 'haskell' }
Plug 'alx741/vim-hindent' , { 'for': 'haskell' }
" " Errors highlighting
Plug 'dense-analysis/ale' , { 'for': 'haskell' }
" " Syntax highlighting
Plug 'neovimhaskell/haskell-vim' , { 'for': 'haskell' }
" " noseke de que se actualiza solo haskell
" Plug 'bitc/vim-hdevtools'

" HTML + php
Plug 'captbaritone/better-indent-support-for-php-with-html'

" Vim Orgmode
Plug 'jceb/vim-orgmode'
Plug 'dhruvasagar/vim-table-mode'
Plug 'tpope/vim-speeddating'

call plug#end()

let g:airline_theme='violet'
let g:airline_powerline_fonts = 1

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => NERDTree
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Uncomment to autostart the NERDTree
" autocmd vimenter * NERDTree
map <C-n> :NERDTreeToggle<CR>
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'
let NERDTreeShowLineNumbers=1
let NERDTreeShowHidden=1
let NERDTreeMinimalUI = 1

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
hi LineNr ctermfg=242
hi CursorLineNr ctermfg=15
hi VertSplit ctermfg=8 ctermbg=0
hi Statement ctermfg=3

" Settings

" LaTeX
let g:tex_flavor='latex'

" Numbers
:set number
:set relativenumber

" Color scheme + syntax
syntax on
colorscheme gruvbox

" CUSTOM CONFIG
" Search down in subforlders
set path+=**
set nocompatible
set encoding=utf-8
" syntax enable
filetype on
filetype plugin indent on
set autoindent
set laststatus=2
set incsearch
set hlsearch
set cursorline

" Didsplay all atvhing files when we tab complete
set wildmenu

" map <C-p> <C-]>

" TAGS
 " Create the 'tags' file (may need to install ctags first)
let g:autotagTagsFile=".tags"
 " JAVA
command! MakeTags !ctags -R .

" Recompile groff document on write
autocmd BufWritePost *.ms | :!groff -ms %:p -ep -T pdf > %:r.pdf

 " C
" set termguicolors 
set t_Co=256
let mapleader=","


" Folds
set foldlevelstart=0
" set nofoldenable
set foldmethod=indent
set foldnestmax=10

" XML/HTML
imap <silent> <C-c> </<C-X><C-O><C-X><Esc>xa
nmap <silent> <leader>c a</<C-X><C-O><C-X><Esc>x


""Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
""If you're using tmux version 2.2 or later, you can remove the outermost
""$TMUX check and use tmux's 24-bit color support
""(see < http://sunaku.github.io/tmux-24bit-color.html#usage > for more
""information.)
"if (empty($TMUX))
"  if (has("nvim"))
"    "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
"    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
"  endif
"  "For Neovim > 0.1.5 and Vim > patch 7.4.1799 <
"  "https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
"  "Based on Vim patch 7.4.1770 (`guicolors` option) <
"  "https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
"  " < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
"  if (has("termguicolors"))
"    set termguicolors
"  endif
"endif

"Remap ESC to ii
:imap ii <Esc>

" tab navigation mappings
map gn :tabnew 

" better backup, swap and undos storage
set directory=~/.vim/dirs/tmp     " directory to place swap files in
set backup                        " make backup files
set backupdir=~/.vim/dirs/backups " where to put backup files
set undofile                      " persistent undos - undo after you re-open the file
set undodir=~/.vim/dirs/undos
set viminfo+=n~/.vim/dirs/viminfo
" store yankring history file there too
let g:yankring_history_dir = '~/.vim/dirs/'

" create needed directories if they don't exist
if !isdirectory(&backupdir)
    call mkdir(&backupdir, "p")
endif
if !isdirectory(&directory)
    call mkdir(&directory, "p")
endif
if !isdirectory(&undodir)
    call mkdir(&undodir, "p")
endif

" toggle nerdtree display
map <F3> :NERDTreeToggle<CR>
" open nerdtree with the current file selected
" nmap <leader>t :NERDTreeFind<CR>
" don;t show these file types
let NERDTreeIgnore = ['\.pyc$', '\.pyo$']


" Lo de los brackets y parentesis
inoremap ( ()<Esc>i
inoremap [ []<Esc>i
inoremap { {}<Esc>i
autocmd Syntax html,vim inoremap < <lt>><Esc>i| inoremap > <c-r>=ClosePair('>')<CR>
inoremap ) <c-r>=ClosePair(')')<CR>
inoremap ] <c-r>=ClosePair(']')<CR>
" inoremap } <c-r>=CloseBracket()<CR>
inoremap } <c-r>=ClosePair('}')<CR>
" inoremap " <c-r>=QuoteDelim('"')<CR>
" inoremap ' <c-r>=QuoteDelim("'")<CR>

function ClosePair(char)
 if getline('.')[col('.') - 1] == a:char
 return "\<Right>"
 else
 return a:char
 endif
endf

function CloseBracket()
 if match(getline(line('.') + 1), '\s*}') < 0
 return "\<CR>}"
 else
 return "\<Esc>j0f}a"
 endif
endf

function QuoteDelim(char)
 let line = getline('.')
 let col = col('.')
 if line[col - 2] == "\\"
 "Inserting a quoted quotation mark into the string
 return a:char
 elseif line[col - 1] == a:char
 "Escaping out of the string
 return "\<Right>"
 else
 "Starting a string
 return a:char.a:char."\<Esc>i"
 endif
endf


" tab length exceptions on some file types
autocmd FileType html setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType php setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType javascript setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType htmldjango setlocal shiftwidth=4 tabstop=4 softtabstop=4
autocmd FileType haskell setlocal shiftwidth=2 tabstop=2 softtabstop=2

let g:pymode_rope = 1
let g:pymode_rope_completion = 1
let g:pymode_indent = 1
let g:pymode_motion = 1
let g:pymode_run = 0
let g:pymode_syntax = 1
let g:pymode_syntax_all = 1
let g:pymode_syntax_print_as_function = 1
let g:pymode_syntax_indent_errors = g:pymode_syntax_all
let g:pymode_syntax_space_errors = g:pymode_syntax_all
let g:pymode_syntax_highlight_exceptions = g:pymode_syntax_all
"let g:pymode_rope_project_root += %:p:h
""let g:pymode_rope_rename_bind = '<C-x><C-x>'
let g:pymode_python = 'python'
" nnoremap <leader>p let g:pymode_python = 'python'
" nnoremap <leader>P let g:pymode_python = 'python3'
" noremap <buffer> <leader>r :exec '!python' shellescape(@%, 1)<cr>

let g:gruvbox_contrast_dark='medium'
let g:gruvbox_contrast_light='medium'
set background=dark

nnoremap <silent> [oh :call gruvbox#hls_show()<CR>
nnoremap <silent> ]oh :call gruvbox#hls_hide()<CR>
nnoremap <silent> coh :call gruvbox#hls_toggle()<CR>

nnoremap * :let @/ = ""<CR>:call gruvbox#hls_show()<CR>*
nnoremap / :let @/ = ""<CR>:call gruvbox#hls_show()<CR>/
nnoremap ? :let @/ = ""<CR>:call gruvbox#hls_show()<CR>?


" set tabstop=4
" set shiftwidth=4
set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab


" mapping to make movements operate on 1 screen line in wrap mode
let b:gmove = "yes"
function! ScreenMovement(movement)
  if exists("b:gmove") && &wrap && b:gmove == 'yes'
    return "g" . a:movement
  else
    return a:movement
  endif
endfunction
onoremap <silent> <expr> j ScreenMovement("j")
onoremap <silent> <expr> k ScreenMovement("k")
onoremap <silent> <expr> 0 ScreenMovement("0")
onoremap <silent> <expr> ^ ScreenMovement("^")
onoremap <silent> <expr> $ ScreenMovement("$")
nnoremap <silent> <expr> j ScreenMovement("j")
nnoremap <silent> <expr> k ScreenMovement("k")
nnoremap <silent> <expr> 0 ScreenMovement("0")
nnoremap <silent> <expr> ^ ScreenMovement("^")
nnoremap <silent> <expr> $ ScreenMovement("$")
vnoremap <silent> <expr> j ScreenMovement("j")
vnoremap <silent> <expr> k ScreenMovement("k")
vnoremap <silent> <expr> 0 ScreenMovement("0")
vnoremap <silent> <expr> ^ ScreenMovement("^")
vnoremap <silent> <expr> $ ScreenMovement("$")
vnoremap <silent> <expr> j ScreenMovement("j")
" toggle showbreak
function! TYShowBreak()
  if &showbreak == ''
    set showbreak=>
  else
    set showbreak=
  endif
endfunction
function! TYToggleBreakMove()
  if exists("b:gmove") && b:gmove == "yes"
    let b:gmove = "no"
  else
    let b:gmove = "yes"
  endif
endfunction
nmap  <expr> ,b  TYShowBreak()
nmap  <expr> ,bb  TYToggleBreakMove()


nmap <leader>u :w<CR>:!dos2unix %:p<CR>:edit<CR>
nmap <leader>d :w<CR>:!unix2dos %:p<CR>:edit<CR>

if exists('+colorcolumn')
    set colorcolumn=80
endif

set textwidth=80

" Red after 80th character
au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1)

set tabpagemax=64

au BufNewFile,BufRead *.ms set filetype=groff

if &term =~ '256color'
    " disable Background Color Erase (BCE) so that color schemes
    " render properly when inside 256-color tmux and GNU screen.
    " see also http://sunaku.github.io/vim-256color-bce.html
    set t_ut=
endif

" File browser built-in configs
let g:netrw_banner=0
let g:netrw_browse_split=0
let g:netrw_altv=1
let g:netrw_liststyle=3
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide.=',\(^\|\s\s\)\zs\.\S\+'

" Show commands
set showcmd

" Haskell
let g:haskell_enable_quantification = 1   " to enable highlighting of `forall`
let g:haskell_enable_recursivedo = 1      " to enable highlighting of `mdo` and `rec`
let g:haskell_enable_arrowsyntax = 1      " to enable highlighting of `proc`
let g:haskell_enable_pattern_synonyms = 1 " to enable highlighting of `pattern`
let g:haskell_enable_typeroles = 1        " to enable highlighting of type roles
let g:haskell_enable_static_pointers = 1  " to enable highlighting of `static`
let g:haskell_backpack = 1                " to enable highlighting of backpack keywords
let g:haskell_classic_highlighting = 0    " Haskell classic highlighting

" Reload documents
nnoremap <silent> <leader>r :edit!<CR>

" Markdown preview
map <C-l> :w!<CR>:w!/home/initega/tmp/vim-markdown.md<CR>:!grip %:p --export /home/initega/tmp/vim-markdown.html<CR>:!firefox-bin /home/initega/tmp/vim-markdown.html > /dev/null 2> /dev/null &<CR><CR>
