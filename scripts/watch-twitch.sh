#!/usr/bin/env sh


if [ -z "$1" ]; then

    while read -r streamer; do
        if [ "$(youtube-dl -e "https://www.twitch.tv/$streamer" 2>/dev/null)" ]
        then
            online_streamers="$online_streamers\n$streamer"
        fi
    done < "$FAV_STREAMERS"

    if [ -z "${online_streamers}" ]; then
        notify-send "No Streamers Found" "watch-twitch.sh"
    else
        online_streamers=$(printf "%b\n" "$online_streamers" | sed "1d")
        watch="$(printf "%b\n" "${online_streamers}" | rofi -dmenu -p "Streamer name")"
        printf "%b\n" "${online_streamers}" > "$HOME/.local/share/watch-twitch.streams"
    fi


else

    watch="$1"

fi

[ -n "$watch" ] && {
    dash $TSCRIPTS/watch-video-select-format.sh "https://www.twitch.tv/$watch"
}
