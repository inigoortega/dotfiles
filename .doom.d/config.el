(setq user-full-name "inigoortega"
      user-mail-address "inigoortega@tutanota.com")
;; -*- lexical-binding: t -*-
(add-to-list 'load-path "/usr/share/emacs/site-lisp/ebuild-mode")
(require 'ebuild-mode)

(add-to-list 'load-path "/usr/share/emacs/site-lisp/color-theme")
(require 'color-theme)
;; (add-to-list 'auto-mode-alist '("\\.conf$" . shell-script-mode)) ;Configuration files.
;; (load-directory (file-expand-wildcards "/usr/share/emacs/site-lisp/ebuild-mode"))
(setq doom-font (font-spec :family "monospace" :width 'normal :size 17)
      doom-variable-pitch-font (font-spec :family "monospace"))
(setq doom-theme 'doom-gruvbox)
(setq display-line-numbers-type 'relative)
(setq org-directory "~/org/")
(setq sh-symbols
      '(sed tail printf echo pidof top dmenu rofi killall sed awk tee basename
            ln mkdir rm ssh sleep source ps bash python perl Rscript wget
            bunzip2 bzip2 zip unzip gzip gunzip find ls cat egrep grep mv cp
            chmod tar stty export spark-shell spark-submit hadoop pyspark aws
            dash xdotool xprop 7z p7z zsh fish expr command disown pv alias head
            eselect equery qlist emerge
            ))

(setq prelude-symbols
      '(not otherwise maybe either fst snd curry uncurry compare min max succ
            pred toEnum fromEnum enumFrom enumFromThen enumFromTo enumFromThenTo
            minBound maxBound abs negate signum fromInteger show showsPrec showList
            toInteger toRational quot rem div mod quotRem divMod toInteger recip
            fromRational pi exp log sqrt logBase sin cos tan asin acos atan sinh cosh
            tanh asinh acosh atanh map properFraction truncate round ceiling floor
            floatRadix floatDigits floatRange decodeFloat encodeFloat exponent
            significand scaleFloat isNaN isInfinite isDenormalized isNegativeZero
            isIEEE atan2 subtract odd even gcd lcm fromIntegral realToFrac mempty
            mappend mconcat fmap pure return fail mapM_ sequence foldMap foldr foldl
            foldr1 foldl1 elem minimum sum product traverse sequenceA mapM id const
            flip until asTypeOf error errorWithoutStackTrace undefined seq filter head
            last tail init null length reverse and or any all concat concatMap scanl
            scanr scanr1 iterate repeat replicate cycle take drop dropWhile takeWhile
            splitAt span break notElem lookup zip zip3 zipWith zipWith3 unzip unzip3
            lines words unlines unwords shows showChar showString showParen readsPrec
            readList reads readParen read lex putChar putStr putStrLn print getChar
            getLine getContents interact readFile writeFile appendFile readIO readLn
            ioError userError))

(setq key-commands '(("i !" "(insert! \"i!\")")
                     ("i \\\"" "(insert! (format \"i%c\" 34))")
                     ("i SPC" "(insert! \"i \")")
                     ("i ESC" "(insert! \"i\") (evil-normal-state)")
                     ("i RET" "(insert! \"i\") (execute-kbd-macro (kbd \"RET\"))")
                     ("i \\\\" "(insert! \"i\\\\\")")
                     ("i DEL" "(insert! \"\")")
                     ("i TAB" "(insert! \"i\")
                   (execute-kbd-macro (kbd \"TAB\"))")
                     ("i C-n" "(insert! \"i\") (evil-complete-next)")
                     ("i <f1>" "(insert! \"i\") (execute-kbd-macro (kbd \"<f1>\"))")
                     ("i <f2>" "(insert! \"i\") (execute-kbd-macro (kbd \"<f2>\"))")
                     ("i <f3>" "(insert! \"i\") (execute-kbd-macro (kbd \"<f3>\"))")
                     ("i <f4>" "(insert! \"i\") (execute-kbd-macro (kbd \"<f4>\"))")
                     ("i <f5>" "(insert! \"i\") (execute-kbd-macro (kbd \"<f5>\"))")
                     ("i <f6>" "(insert! \"i\") (execute-kbd-macro (kbd \"<f6>\"))")
                     ("i <f7>" "(insert! \"i\") (execute-kbd-macro (kbd \"<f7>\"))")
                     ("i <f8>" "(insert! \"i\") (execute-kbd-macro (kbd \"<f8>\"))")
                     ("i <f9>" "(insert! \"i\") (execute-kbd-macro (kbd \"<f9>\"))")
                     ("i <f10>" "(insert! \"i\") (execute-kbd-macro (kbd \"<f10>\"))")
                     ("i <f11>" "(insert! \"i\") (execute-kbd-macro (kbd \"<f11>\"))")
                     ("i <f12>" "(insert! \"i\") (execute-kbd-macro (kbd \"<f12>\"))")
                     ("i <Scroll_Lock>" "(insert! \"i\") (execute-kbd-macro (kbd \"<Scroll_Lock>\"))")
                     ("i <insert>" "(insert! \"i\") (execute-kbd-macro (kbd \"<insert>\"))")
                     ("i <deletechar>" "(insert! \"i\") (execute-kbd-macro (kbd \"<deletechar>\"))")
                     ("i <home>" "(insert! \"i\") (execute-kbd-macro (kbd \"<home>\"))")
                     ("i <end>" "(insert! \"i\") (execute-kbd-macro (kbd \"<end>\"))")
                     ("i <prior>" "(insert! \"i\") (execute-kbd-macro (kbd \"<prior>\"))")
                     ("i <next>" "(insert! \"i\") (execute-kbd-macro (kbd \"<next>\"))")
                     ))

(setq path-to-ctags "/usr/bin/ctags")

(setq i-keys (mapcar
              (lambda (number) (setq command (format "i%c" number)))
              (number-sequence 35 91)))
(nconc i-keys (mapcar
               (lambda (number) (setq command (format "i%c" number)))
               (number-sequence 93 250)))
(defun eval-string (string)
  "Evals all the commands inside the string"
  (eval (car (read-from-string (format "(progn %s)" string)))))

;;; Shell script highlighting
(defun highlight-words-on-mode (mode symbols &optional face)
  "Makes given symbols be highlighted on given mode"
  (setq strings (mapcar 'symbol-name symbols))
  (setq formatted-words
        (concat "\\\\<\\\\(" (mapconcat 'identity
                                        strings "\\\\|") "\\\\)\\\\>"))
  (if face
      (eval-string (format "(font-lock-add-keywords '%s '((\"%s\" .
%s)))" mode formatted-words face))
    (eval-string (format "(font-lock-add-keywords '%s '((\"%s\" .
%s)))" mode formatted-words font-lock-builtin-face))
    )

  ;; highlight options
  (font-lock-add-keywords
   mode
   '(("\\<-[-a-zA-Z0-9]+\\>"
      . font-lock-constant-face)))
  (font-lock-add-keywords
   mode
   `((,(concat "[ \t]+"
               (regexp-opt
                '("!" "!=" "==" "=~") 'paren) "[ \t]+")
      . font-lock-constant-face)))
  )

(defun create-tags (dir-name)
  "Create tags file."
  (interactive "DDirectory: ")
  (shell-command
   (format "%s -f TAGS -e -R %s" path-to-ctags (directory-file-name dir-name)))
  )
(map! :map org-mode-map
      :n "z M" (lambda () (interactive) (org-shifttab) (+fold/close-all)))
;; (map! :map org-mode-map
;;       :n "z m" (lambda () (interactive) (org-shifttab) (+fold/close-all)))
(map! :n "g r" #'evil-replace-with-register)
(add-hook! 'org-mode-hook #'evil-replace-with-register)
(map! :n "SPC w D" #'delete-other-windows)
(defvar my-term-shell "/bin/zsh")
(defadvice ansi-term (before force-bash)
  (interactive (list my-term-shell)))
(ad-activate 'ansi-term)
(setq ii-maps '(evil-org-mode-map text-mode-map prog-mode-map))
(mapcar
 (lambda (command)
   (eval-string
    (format "(map!
:map %s
:i \"%s\" (lambda() (interactive) (insert! \"%s\")))"
            ii-maps command command))) i-keys)

(mapcar
 (lambda (key-command)
   (eval-string
(format
"
(map! :map %s
      :i \"%s\" (lambda()
                 (interactive)
                 %s
))
"
ii-maps (nth 0 key-command) (nth 1 key-command)))) key-commands)


;; i i
(eval-string
(format
"(map! :map %s
      :i \"i i\" #'evil-normal-state)" ii-maps))
(setq org-display-inline-images t)
(setq org-redisplay-inline-images t)
(setq org-startup-with-inline-images "inlineimages")
(require 'ox-html)
(require 'ox-latex)

;; Path for pygments or command name
(defvar pygments-path "pygmentize")

;; (setq org-latex-listings 'minted)
;; (setq org-latex-packages-alist nil)
;; (add-to-list 'org-latex-packages-alist '("cache=false" "newfloat" "minted"))
;; inside .emacs file
(setq org-latex-listings 'minted
      org-latex-packages-alist '(("newfloat" "minted"))
      org-latex-minted-options
          '(("frame" "single")
            ;; ("frame" "lines")
            ("linenos" "true")
           )
      org-latex-pdf-process
      '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
        "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
        "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))
;; (add-to-list 'org-latex-packages-alist '("babel" t ("pdflatex")))
;; (setq org-latex-pdf-process nil)
;; (add-to-list 'org-latex-pdf-process
;;              "pdflatex -interaction nonstopmode --shell-escape -output-directory %o %f")
(setq )
(setq org-export-latex-listings 'minted)
(setq org-export-latex-packages-alist nil)
(add-to-list 'org-export-latex-packages-alist '("newfloat" "minted"))

;; (setq org-latex-packages-alist nil)
(add-to-list 'org-latex-packages-alist
             '("AUTO" "babel" t ("pdflatex") "minted"))
(setq dired-dwim-target t)
(show-paren-mode 1)
(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
(add-hook! 'text-mode-hook #'turn-on-auto-fill)
(setq-default auto-fill-function 'do-auto-fill)
;; (require 'whitespace)
;; (setq whitespace-style '(face lines-tail))
;; (add-hook 'text-mode-hook
;;           (setq whitespace-line-column 80)
;;           ;; (global-whitespace-mode t) ; for dot on spaces
;;           )
;; (setq-default show-trailing-whitespace t)
(setq-default indicate-empty-lines t)
(add-hook 'before-save-hook 'delete-trailing-whitespace)
(global-visual-line-mode)
(global-evil-surround-mode t)
(vimish-fold-global-mode 1)
    (add-hook 'prog-mode-hook
      (lambda ()
      (if (or (derived-mode-p 'python-mode) (derived-mode-p 'haskell-mode))
        (progn
            (yafolding-mode)
            (yafolding-hide-all)
        )
        (+fold/close-all)
    )))

(defun yafolding-hide-rec ()
  (interactive)
  (setq level (yafolding-get-indent-level))
  (while (> level 0)
    (yafolding-go-parent-element)
    (setq level (- level 1)))
  (yafolding-hide-element))

(map! :map 'hs-minor-mode-map
      :n "z o" #'hs-show-block
      )

(map! :map 'yafolding-mode-map
      :n "z a" #'yafolding-toggle-fold
      :n "z c" #'yafolding-hide-parent-element
      :n "z C" #'yafolding-hide-rec
      :n "z o" #'yafolding-show-element
      :n "z O" #'yafolding-show-element
      :n "z m" #'yafolding-hide-all
      :n "z r" #'yafolding-show-all
      )
;; Default user.
(setq rcirc-default-nick "initega")
(setq rcirc-default-user-name "initega")
(setq rcirc-default-full-name "initega")
(after! haskell-mode
  (progn
    (setq
     ;; Format files with Brittany instead of Stylish
     haskell-mode-stylish-haskell-path "brittany"
     ;; Format buffer with Brittany on save
     haskell-stylish-on-save t
     ;; Suggest removing imports
     haskell-process-suggest-remove-import-lines t)))
(add-hook 'haskell-mode-hook
          (lambda ()
            (interactive)
            (haskell-indentation-mode)
            (interactive-haskell-mode)
            (yas-minor-mode)
            ))

(map! :map 'haskell-interactive-mode-map
      :i "C-j" #'haskell-interactive-mode-history-next)
(map! :map 'haskell-interactive-mode-map
      :i "C-k" #'haskell-interactive-mode-history-previous)
;; (setq haskell-process-path-ghci "stack-ghci")
(setq haskell-process-type 'stack-ghci)
(after! dante
  (nconc dante-methods '(stack-ghci))
  (defun dante-repl-command-line ()
    '("stack-ghci"))
  )
;; (setq dante-methods
;; '(styx new-impure-nix new-nix nix impure-nix new-build nix-ghci stack mafia
;; bare-cabal bare-ghci)))

(add-hook 'dante-mode-hook
          '(lambda () (flycheck-add-next-checker 'haskell-dante
                                                 '(warning . haskell-hlint))))
(highlight-words-on-mode 'haskell-mode prelude-symbols)
(setq org-babel-load-languages '((emacs-lisp . t) (ein . t)))
(elpy-enable)

;;; Use IPython for REPL
(setq python-shell-interpreter "jupyter"
      python-shell-interpreter-args "console --simple-prompt"
      python-shell-prompt-detect-failure-warning nil)
(add-to-list 'python-shell-completion-native-disabled-interpreters
             "jupyter")
(add-hook 'python-mode-hook
      (lambda ()
        (setq-default indent-tabs-mode nil)
        (setq-default tab-width 4)
        (setq-default python-indent 4)))
(highlight-words-on-mode 'sh-mode sh-symbols)
(map! :map 'pdf-links-minor-mode-map
      :n "f" #'pdf-links-action-perform
      )

(map! :map 'pdf-view-mode-map
      :n "C d" #'pdf-links-action-perform
      )
