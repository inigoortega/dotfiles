#!/usr/bin/env sh

file="/tmp/lock-image-$(date +%s).png"

import -window root "$file"
pixelize -m 2 -s 7 "$file" "$file"
feh -F "$file" &
