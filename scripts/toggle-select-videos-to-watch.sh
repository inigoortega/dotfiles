#!/usr/bin/env sh

stop_file="/tmp/stop-select-videos-to-watch"

if [ ! -e $stop_file ]; then
    notify-send "Looking" "Select Video to Watch"
    dash $SCRIPTS/select-videos-to-watch.sh
else
    notify-send "Stop Looking"
    dash $SCRIPTS/stop-select-videos-to-watch.sh
fi
