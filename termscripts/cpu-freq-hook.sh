#!/usr/bin/env sh

iter="1"
while [ $iter -eq "1" ];
do
    freq="$(cat /tmp/cpu-freq)"
    iter="$(echo "$freq > 5" | bc -l)"
    if [ $iter -eq 0 ]; then
        sleep 5m
        freq="$(cat /tmp/cpu-freq)"
        iter="$(echo "$freq > 5" | bc -l)"
    else
        sleep 2m
    fi
done 

printf "%b\n" "done"
sleep 10m

$1
