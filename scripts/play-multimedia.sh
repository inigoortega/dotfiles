#!/usr/bin/env sh

formats="$(dash -c "printf \"%b\n\" \$$(printf "%b\n" $1)_FORMATS")"
player="$(dash -c "printf \"%b\n\" \$$(printf "%b\n" $1)PLAYER")"

formats="$(printf "%b\n" "$formats" | sed 's/,/\\|/g')"

regex=".*\\.\($formats\)"

trash="\($(printf "%b\n" "$TRASH" | sed 's/:/\/.*\\|/')/.*\)"

multimedia="$(find "$HOME" -regex "$regex" -not -regex "$trash")"
multimedia="$(printf "%b\n" "$multimedia" | sed "s|^$HOME/||")"

multimedia="${multimedia}$(find "$DEVICES_FOLDER" -regex "$regex" \
    -not -regex "$trash")"

media="$(printf "%b\n" "${multimedia}" | dmenu -i -l 9)"

[ "$(printf "%b\n" "$media" | grep -E "$DEVICES_FOLDER/.*" | wc -l)" -eq 0 ] &&
    media="$HOME/$media"

[ "$media" != "$HOME/" ] && dash -c "$player \"$media\""
