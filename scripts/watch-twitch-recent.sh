#!/usr/bin/env sh

watch=$(rofi -dmenu -p "Streamer name" < "$HOME/.local/share/watch-twitch.streams")

[ -n "$watch" ] && {
    dash $TSCRIPTS/watch-video-select-format.sh "https://www.twitch.tv/$watch"
}
