#!/usr/bin/env sh

cleanup() {
    killall youtube-dl
}
trap cleanup INT TERM

usage() {
    echo "Usage: $0 [-f <file>] [-u] [-h] [-p]" 1>&2
    echo "" 1>&2
    printf \
        "\tThis program manages your subscriptions with the given file with -f or\n" 1>&2
    printf \
        "\tby default using \$YT_SUBS or \$HOME/yt-subs, respectively.\n\n" 1>&2
    printf "\t-u  To update now previous video upload speeds\n" 1>&2
    printf \
        "\t-e  To play the videos when at the end instead of progressivaly\n" 1>&2
    printf "\t-h  Show this help text\n" 1>&2
    printf "\t-p  Preserve old wlvfs_videos file.\n" 1>&2
    exit 1
}

while getopts ":upef:" option; do
    case "${option}" in
        u)
            update_now="1"
            ;;
        e)
            play_at_the_end="1"
            ;;
        f)
            subs_file="${OPTARG}"
            ;;
        p)
            preserve_oldvideos="1"
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

videos_file="$HOME/.local/share/wlvfs_videos"

if [ -z "$subs_file" ]; then
    if [ -z "$YT_SUBS" ]; then
        subs_file="$HOME/yt-subs"
    else
        subs_file="$YT_SUBS"
    fi
fi

already_updated=$(grep "^!updated" "$subs_file")

if [ "$(echo "$(date +%j) % 14" | bc)" -ne 0 ] && [ "$already_updated" ]; then
    sed -i '/!updated/d' "$subs_file"
fi

# In order to print it too on a file, so that I can track where it is
{

    # Preprocessing

    grep -vE '^#|^$|^!' "$subs_file" | while read line; do
        channel=$(echo "$line" | awk '{print $1}')
        last_video=$(echo "$line" | awk '{print $2}')
        update_date=$(echo "$line" | awk '{print $3}')
        upload_frequency=$(echo "$line" | awk '{print $4}')
        prev_upload_frequency=$(echo "$line" | awk '{print $5}')
        manual_preference=$(echo "$line" | awk '{print $6}')

        new_last_video="$last_video"
        new_update_date="$update_date"
        new_upload_frequency="$upload_frequency"
        new_prev_upload_frequency="$prev_upload_frequency"
        new_manual_preference="$manual_preference"

        num_w=$(echo "$line" | wc -w)

        [ "$num_w" -lt 6 ] && {
            echo "Preprocessing: $channel"

            case $num_w in
                1)
                    new_last_video=$(youtube-dl --get-id \
                        "https://invidio.us/channel/$channel" 2>/dev/null \
                        | head -n 1)


                    if [ -z "$new_last_video" ]; then
                        new_last_video="-"
                        new_update_date="-"
                    else
                        new_update_date=$(dash $TSCRIPTS/video-upload-date.sh \
                            "$new_last_video")
                    fi
                    new_upload_frequency=0
                    new_prev_upload_frequency=0
                    new_manual_preference=0
                    ;;
                2)
                    if [ "$last_video" = "-" ]; then
                        new_last_video=$(youtube-dl --get-id \
                            "https://invidio.us/channel/$channel" 2>/dev/null \
                            | head -n 1)
                        [ -z "$new_last_video" ] && new_last_video="-"
                        new_update_date=$(dash $TSCRIPTS/video-upload-date.sh \
                            "$new_last_video")
                        new_upload_frequency=0
                        new_prev_upload_frequency=0
                        new_manual_preference=0
                    else
                        new_update_date=$(dash $TSCRIPTS/video-upload-date.sh \
                            "$new_last_video")
                        new_upload_frequency=0
                        new_prev_upload_frequency=0
                        new_manual_preference=0
                    fi
                    ;;
                3)
                    new_upload_frequency=0
                    new_prev_upload_frequency=0
                    new_manual_preference=0

                    ;;
                4)
                    new_prev_upload_frequency=0
                    new_manual_preference=0

                    ;;
                5)
                    new_manual_preference=0

                    ;;
            esac

            sed -E "s|($channel)(\t$last_video)?(\t$update_date)?(\t$upload_frequency)?(\t$prev_upload_frequency)?(\t$manual_preference)?|$channel\t$new_last_video\t$new_update_date\t$new_upload_frequency\t$new_prev_upload_frequency\t$new_manual_preference|" "$subs_file" -i
        }
    done


    # Processing
    [ -z "$preserve_oldvideos" ] && rm "$videos_file" 2> /dev/null

    grep -vE '^#|^$|^!' "$subs_file" | sort -rg -k 6 -k 5 -k 4 | while read line; do
        channel=$(echo "$line" | awk '{print $1}')
        last_video=$(echo "$line" | awk '{print $2}')
        update_date=$(echo "$line" | awk '{print $3}')
        upload_frequency=$(echo "$line" | awk '{print $4}')
        prev_upload_frequency=$(echo "$line" | awk '{print $5}')
        manual_preference=$(echo "$line" | awk '{print $6}')

        echo "Processing: $channel on Preference: $manual_preference - $prev_upload_frequency"

        temp="/tmp/wlvfs"
        dateafter="$update_date"
        [ "$update_date" = "-" ] && dateafter=""

        dash $TSCRIPTS/channel-videos-after.sh \
            "https://youtube.com/channel/$channel" "$last_video" "$dateafter" |
            tee "$temp" |
            while read video; do
                printf "\tFound video: %s\n" "$video"
                if [ ! -f "$videos_file" ]; then
                    echo "ytdl://$video" >> $videos_file
                    if [ -z "$play_at_the_end" ]; then
                        (dash $TSCRIPTS/play-videos-from-file.sh "$videos_file"&&
                             rm "$videos_file") &
                    fi
                else
                    echo "ytdl://$video" >> $videos_file
                    {
                        sleep 5
                        [ ! -f "$videos_file" ] && echo "ytdl://$video" >> \
                            $videos_file
                    } &
                fi
            done
        new_last_video=$(head -n 1 "$temp")
        if [ -z "$new_last_video" ]; then
            new_last_video="$last_video"
            new_upload_frequency="$upload_frequency"
            new_update_date="$update_date"
        else
            new_update_date=$(dash "$TSCRIPTS/video-upload-date.sh" "$new_last_video")
            new_upload_frequency=$(( $(wc -l "$temp" | \
                awk '{print $1}') + $upload_frequency ))
        fi

        if [ "$update_now" ] || \
            { [ "$(echo "$(date +%j) % 14" | bc)" -eq 0 ] && [ ! "$already_updated" ]; }
        then
            sed \
                "s|$channel\t$last_video\t$update_date\t$upload_frequency\t$prev_upload_frequency|$channel\t$new_last_video\t$new_update_date\t0\t$new_upload_frequency|" \
                "$subs_file" -i
        else
            sed \
                "s|$channel\t$last_video\t$update_date\t$upload_frequency\t$prev_upload_frequency|$channel\t$new_last_video\t$new_update_date\t$new_upload_frequency\t$prev_upload_frequency|" \
                "$subs_file" -i
        fi

    done

} | tee "/tmp/wlvfs-log"


if [ "$(echo "$(date +%j) % 14" | bc)" -eq 0 ] && [ ! "$already_updated" ]; then
    echo "!updated" >> "$subs_file"
fi

notify-send "Watch Subs" "Done"
