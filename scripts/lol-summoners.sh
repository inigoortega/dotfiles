#!/usr/bin/env sh

# ISC License

# Copyright (c) 2019, Iñigo Ortega

# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.

# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


# Help menu
usage() {
    printf "%b\n" "Usage: $0 [-m] [-i] [-h] [-p] [-b <browser>] [-r <region>] \
[-s <summoner name>]" 1>&2
    printf "%b\n" "
-h  Print this help menu
-m  Also opens your summmoner's op.gg
-i  Reads from standard input instead of clipboard
-p  Read from primary selection instead of clipboard
-b <browser>  Sets the browser to use when openning op.gg
              If not provided, uses \$BROWSER or the default browser instead.
-r <region>  Sets the region (server) to search summoners on
             If not provided, uses \$LOL_REGION instead.
-s <summoner name>  Use provided summoner names as yours. Delimitate each
summoner name with ':', such as, \"hi im gosu:hide on bush:brian\"
                    If not provided, uses \$LOL_SUMMONERS instead.

Made for r/leagueoflinux by Iñigo Ortega
" >&2
    exit 1
}

# Gets the browser that will be used
get_browser() {
    # Check for environment variable
    if [ -n "$BROWSER" ]; then
        browser="$BROWSER"
    else
        # Check for Mime app
        if [ $(command -v xdg-open) ]; then
            browser="xdg-open"
        else
            # Check for python
            if [ $(command -v python) ]; then
                browser="python -m webbrowser"
            else
                # Error
                printf "%b\n" "Can't open any browser'" 1>&2
                exit 2
            fi
        fi
    fi

    printf "%b\n" "$browser"
}

# To get options provided to the command
while getopts ":mb:r:s:ihp" option; do
    case "${option}" in
        m)
            no_ignore_me="1"
            ;;
        i)
            stdin="1"
            ;;
        p)
            selection="primary"
            ;;
        r)
            region="${OPTARG}"
            ;;
        s)
            my_summoners="$(printf "%b\n" "${OPTARG}" | sed "s/:/\n/")"
            ;;
        b)
            browser="${OPTARG}"
            if [ ! command -v "$browser" ]; then
                printf "%b\n" "Can't run \'$browser\' browser'" 1>&2
                exit 3
            fi
            ;;
        * | h)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

# The text that is on the right of summoner name
pattern=" joined the lobby\$"

# Temporal file for the while afterward
temp="/tmp/lol-summmoners.$(date +%s)"

# Get summoners from the -s option or $LOL_SUMMONERS env variable
if [ -z "$my_summoners" ]; then
    my_summoners="$(printf "%b\n" "$LOL_SUMMONERS" | sed "s/:/\n/")"
fi

# Get the text copied from champion select
if [ -z "$stdin" ]; then
    if [ $(command -v xclip) ]; then
        text="$(xclip -o -selection ${selection:-clipboard})"
    else
        printf "%b\n" "xclip not found!" 1>&2
        exit 4
    fi
else
    text="$(cat -)"
fi

# A simple check before reading the text
if [ "$(printf "%b\n" "$text" | wc -l)" -lt 3 ]; then
    printf "%b\n" "Are you sure this is correct?\n\n" 1>&2
    printf "%b\n" "$text\n"
fi

# Get summoner names and save them into the temporary file
printf "%b\n" "$text" | grep "$pattern" | sed -E "s/(.*)$pattern/\1/" > "$temp"

# Region
if [ -z "$region" ]; then
    region="$LOL_REGION"
    [ -z "$region" ] && region="euw"
fi

# Browser
if [ -z "$browser" ]; then
    browser="$(get_browser)"
fi

# For each summoner, open a tab with op.gg
while read -r summoner
do
    # This variable is non empty if it is one of your summoner names
    my_summoner="$(printf "%b\n" "$my_summoners" | grep "$summoner")"

    # By default, ignore if it is you. With -m option, include you
    if [ -n "$my_summoner" -a -z "$no_ignore_me" ]; then
        continue
    fi

    # Make spaces +
    summoner="$(printf "%b\n" "$summoner" | sed "s/ /+/g")"
    # Open
    $browser "https://$region.op.gg/summoner/userName=$summoner"
done < "$temp"

rm "$temp"
