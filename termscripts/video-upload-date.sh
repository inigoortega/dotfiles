#!/usr/bin/env bash

secs="$(date +%s%N)"
file_prefix="/tmp/wlvfs$secs-"

fifo="/tmp/fifo-vup$secs"
mkfifo "$fifo"
youtube-dl -o "$file_prefix%(upload_date)s" -- "$1" 2>/dev/null > "$fifo" &
pid="$!"

while read -r line; do
    file=$(echo "$line" | grep 'Destination:')
    [ -n "$file" ] && {
        file=$(echo "$file" | cut -d " " -f 3)
        echo "$file" | sed -E "s|($file_prefix)([0-9]+)(\..+)?|\2|"
        break
    }
done < "$fifo"

kill "$pid"
rm "$fifo"
echo "$file" | grep -q "^$file_prefix" && rm "$file"* 2>/dev/null
