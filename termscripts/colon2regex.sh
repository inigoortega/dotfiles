#!/usr/bin/env sh

usage() {
    printf "%b\n" "$0 [-r] <Colon separated list>" 1>&2
    printf "%b\n" "[-r]  Adds /.* at the end of each entry as if they were folders" 1>&2
    exit 1
}

# To get options provided to the command
while getopts ":r" option; do
    case "${option}" in
        r)
            recursive="1"
            ;;
        * | h)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

[ -z "$1" ] && exit 1

if [ "$recursive" = "1" ]; then
    printf "%b\n" "\($1/.*\)" | sed 's/:/\/.*\\|/g'
else
    printf "%b\n" "\($1\)" | sed 's/:/\\|/g'
fi
