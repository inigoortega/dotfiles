#!/usr/bin/env sh

script="$(find "$SCRIPTS/" -type f -name "*.sh" | sed "s|^$SCRIPTS/||" | dmenu)"
script="$SCRIPTS/$script"
dash $script
