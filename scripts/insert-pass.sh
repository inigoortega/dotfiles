#!/usr/bin/env sh

page="$(find "$PASSWORD_STORE_DIR/" -not -regex "$PASSWORD_STORE_DIR/\\..*" \
    -type d | sed -r "s|([^/])$|\1/|")"
page="${page}\n$(find "$PASSWORD_STORE_DIR/" -not -regex \
    "$PASSWORD_STORE_DIR/\\..*" -type f | sed "s|.gpg$||")"
page_relative="$(printf "%b\n" "$page" | sed "s|$PASSWORD_STORE_DIR||" | \
    grep -E "^(/[^/]+)?(/[^/]+)/?$")"

page_relative="$(printf "%b\n" "$page_relative" | sort | dmenu -l 15 -i)"
[ -z "$page_relative" ] && exit 0
[ -z "$(printf "%b\n" "$page_relative" | grep -E "^(/[^/]+)(/[^/]+)/?$")" ] && \
    notify-send -t 11000 "The reason to show first level folders is to make use\
 of <TAB> on dmenu and create new password inside them. There should NOT be\
 passwords with first level names." && exit 1

page="$PASSWORD_STORE_DIR$page_relative"

username="$(rofi -dmenu -i -p "Username")"

[ -z "$username" ] && exit 0
[ -d "$page" ] && [ -e "$page$username.gpg" ] && notify-send "Account already \
exists" && exit 1

comments="$(printf "%b\n" "Yes\nNo" | rofi -dmenu -i -p "Comments?")"

[ -z "$(printf "%b\n" "$comments" | grep -E "(Yes|No)")" ] && exit 0

generate="$(printf "%b\n" "Yes\nNo" | rofi -dmenu -i -p "Generate password?")"

[ -z "$(printf "%b\n" "$generate" | grep -E "(Yes|No)")" ] && exit 0

contents="login: $username"
if [ "$generate" = "Yes" ]; then
    contents="$(pwgen -ysBv 15 -N 1)\n$contents"
    if [ "$comments" = "Yes" ]; then
        file="/tmp/comments$(date +%s)"
        dash $TSCRIPTS/run-on-terminal.sh $EDITOR $file
        [ ! -f "$file" -o -z "$(cat "$file")" ] && notify-send "Operation CANCELLED" && exit 1
        contents="$contents\ncomments: $(cat "$file")"
        rm "$file"
    fi
    
    if [ -f "$page.gpg" ]; then
        mkdir "$page"
        olduser="$(dash $SCRIPTS/get-pass-username.sh --print "$page_relative")"
        mv "$page.gpg" "$page/$olduser.gpg"
    fi

    [ -d "$page" ] && page_relative="$page_relative/$username"

    printf "%b\n" "$contents" | pass insert -m "$page_relative" || \
        (notify-send "Already exists" && exit 1)
else
    tries=0
    while [ "$tries" -ne 3 ]; do
        passwd="$(rofi -i -dmenu -password -p "Type")"
        passwdcheck="$(rofi -i -dmenu -password -p "Type Again")"
        [ "$passwd" != "$passwdcheck" ] || break \
            && notify-send "Passwords are not equal"
        tries="$(( $tries + 1 ))"
    done
    [ "$passwd" != "$passwdcheck" ] && notify-send "Operation CANCELLED" && \
        exit 1
    contents="$passwd\n$contents"
    if [ "$comments" = "Yes" ]; then
        file="/tmp/comments$(date +%s)"
        dash $TSCRIPTS/run-on-terminal.sh $EDITOR $file
        [ ! -f "$file" -o -z "$(cat "$file")" ] && notify-send "Operation CANCELLED" && exit 1
        contents="$contents\ncomments: $(cat "$file")"
        rm "$file"
    fi

    if [ -f "$page.gpg" ]; then
        mkdir "$page"
        olduser="$(dash $SCRIPTS/get-pass-username.sh --print "$page_relative")"
        mv "$page.gpg" "$page/$olduser.gpg"
    fi

    [ -d "$page" ] && page_relative="$page_relative/$username"

    printf "%b\n" "$contents" | pass insert -m "$page_relative" || \
        (notify-send "Already exists" && exit 1)
fi
