#!/usr/bin/env sh

if [ "$1" -ge 1 ] 2>/dev/null; then
    period="$1"
else
    period="4"
fi

dash $TSCRIPTS/loop.sh -l 50 -p $period "xdotool key ctrl+Tab"
