#!/usr/bin/env sh

p_num=$(pgrep busybox | head)
echo "$p_num"
kill -s 5 $p_num
