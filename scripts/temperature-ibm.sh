#!/usr/bin/env sh

[ ! -e "/proc/acpi/ibm/thermal" ] && exit 1
temps="$(cat /proc/acpi/ibm/thermal)"
temp1="$(printf "%b\n" "$temps" | awk '{print $2}') ºC"
temp2="$(printf "%b\n" "$temps" | awk '{print $5}') ºC"

text="CPU0: $temp1\nCPU1: $temp2"
notify-send "Temperature" "$text"
