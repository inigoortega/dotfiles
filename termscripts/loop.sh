#!/usr/bin/env sh

loops="0"
period="5"

while [ -n "$1" ]; do # while loop starts

	case "$1" in

	-l) 
        loops="$2"
        shift
        ;; 

	-p) 
        period="$2"
        shift
        ;;

	--)
		shift # The double dash which separates options from parameters

		break
		;; # Exit the loop using break command

	*) 
        break
        ;;

	esac

	shift

done

[ "$loops" -eq "0" ] && infinite="true"

while [ $loops -gt 0 -o "$infinite" = "true" ]
do
    dash -c "$@"
    [ "$loops" -gt 0 ] && loops=$(( $loops - 1 ))
    sleep "$period"
done
