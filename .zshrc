# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/home/initega/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="bira"
plugins=(
	git
	chucknorris
	man
	pip
	# cp
	# z
	zsh-autosuggestions
	zsh-syntax-highlighting
	zsh-history-substring-search
	)

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.

source $ZSH/oh-my-zsh.sh
source $HOME/.profile
# source $HOME/ros/serial/setup.zsh
# source /usr/share/rosbash/roszsh

ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=white'

bindkey -v
precmd() { RPROMPT="" }

#export PATH="$PATH:$HOME/bin:$HOME/.local/bin"
#export HOSTNAME="gentoo"
#export SCRIPTS="$HOME/scripts"
#export TSCRIPTS="$HOME/termscripts"
#export WALLPAPERS="$HOME/Pictures/wallpapers"
#export SCREENSHOTS="$HOME/Pictures/Screenshots"
#export EDITOR="vim"
#export VIDEO_FORMATS="mp4,mkv,avi,ogv,webm,divx,wmv,flv"
#export AUDIO_FORMATS="mp3,ogg,vorbis,opus,aac,wav,3gp,mpg,flac"
#export VIDEOPLAYER="mpv"
#export AUDIOPLAYER="cmus-remote"
#export TERMINAL="st"
#export BROWSER="firefox-bin"
#export MUSIC="$HOME/Music/music"
#export PASSWORD_STORE_DIR="$HOME/.password-store"
#export ANDROID_MOUNTPOINT="$HOME/Android"
#export BRIGHTNESS="0.9"
#export SUDO_ASKPASS="/usr/bin/x11-ssh-askpass"
#export TRASH="$HOME/.local/trash:$HOME/.local/share/vifm/Trash"
#export DEVICES_FOLDER="/media"
#export FAV_STREAMERS="$HOME/fav_streamers"
#export LC_ALL=en_US.utf8

##
## ALIASES
#alias ll='lsd --group-dirs first --date relative -alh'
#alias lll='lsd --date relative --group-dirs first -lh'
#alias ls='lsd'
#alias l='/bin/ls'
#alias sc="mksh $SCRIPTS/script-run-fzf.sh"
#alias se="mksh $SCRIPTS/script-edit-fzf.sh"
#alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# neofetch
exec fish
