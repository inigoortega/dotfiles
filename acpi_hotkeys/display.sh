#!/bin/bash

if xset -q | grep -q "DPMS is Disabled"
then
	xset +dpms
fi

if xset -q | grep -q "Monitor is On"
then
	xset dpms force off
else
	xset dpms force on
fi
