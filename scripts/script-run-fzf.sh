#!/usr/bin/env sh


files=$(find "$SCRIPTS/" "$TSCRIPTS/" -regex ".*\.\(ba\)?sh\$" -type f | sed "s|^$HOME/||")
files="${files}\n$(find "$TSCRIPTS/" -regex ".*\.\(ba\)?sh\$" -type l | sed "s|^$HOME/||")"

# printf "%b\n" "${files}"

file="$(printf "%b\n" "${files}" | fzf -i)"
# printf "%b\n" "${file}"

[ -n "$file" ] && {
    if echo "$file" | grep -q "\.bash\$"; then
        bash "$HOME/$file"
    else
        dash "$HOME/$file"
    fi
}
