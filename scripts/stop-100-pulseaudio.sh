#!/usr/bin/env sh

pulseid=$(pidof pulseaudio)

cpupulse=$(top -b -n 1 -p "$pulseid" | tail -n 1 | awk '{print $9}')

stuck=$(printf "%b\n" "$cpupulse > 90" | bc -l)

if [ "$stuck" -eq 1 ]; then
    # stop_pulse=$(printf "%b\n" "Yes\nNo" | dmenu -i -p "Stop PulseAudio?")
    # [ "$stop_pulse" = "Yes" ] && killall pulseaudio
    date >> $HOME/pulseaudio.log
fi
