# ENV=$HOME/.mkshrc; export $ENV
export PATH="$PATH:$HOME/bin:$HOME/.local/bin"
export HOSTNAME="gentoo"
export SCRIPTS="$HOME/scripts"
export TSCRIPTS="$HOME/termscripts"
# export HISTFILE="$HOME/.mksh_history"
export WALLPAPERS="$HOME/Pictures/wallpapers"
export HALLOWEEN_WALLPAPERS="$WALLPAPERS/halloween"
export SCREENSHOTS="$HOME/Pictures/Screenshots"
export EDITOR="vim"
export VIDEO_FORMATS="mp4,mkv,avi,ogv,webm,divx,wmv,flv"
export AUDIO_FORMATS="mp3,ogg,vorbis,opus,aac,wav,3gp,mpg,flac"
export YTDL_FORMAT='bestvideo[height=1080]+bestaudio/bestvideo[height=1440]+bestaudio/[height=1080]/[height=1440]/bestvideo[height<2160]+bestaudio/bestvideo+bestaudio/best'
export VIDEOPLAYER="mpv --ytdl-format=$YTDL_FORMAT"
export VIDEOPLAYER_SELECT_YTDLFORMAT="mpv --ytdl-format="
export AUDIOPLAYER="mpv"
export MUSICPLAYER="mpc"
export MUSICPLAYERUI="ncmpcpp"
export TERMINAL="st"
export BROWSER="firefox-bin"
export MUSIC="$HOME/Music/music"
export PASSWORD_STORE_DIR="$HOME/.password-store"
export ANDROID_MOUNTPOINT="$HOME/Android"
export BRIGHTNESS="0.9"
export SUDO_ASKPASS="/usr/bin/x11-ssh-askpass"
export TRASH="$HOME/.local/trash:$HOME/.local/share/vifm/Trash"
export DEVICES_FOLDER="/media"
export FAV_STREAMERS="$HOME/fav_streamers"
export LC_ALL=en_US.utf8
export KERNEL_CONFIGS="$HOME/kernel-configs"
export TELEGRAM="$HOME/Apps/Telegram/Telegram"

# ROS
export CMAKE_PREFIX_PATH=/usr
export PYTHONPATH=/usr/lib64/python3.6/site-packages/
export ROS_DISTRO=melodic
export ROS_ETC_DIR=/usr/etc/ros
export ROS_MASTER_URI='http://localhost:11311'
export ROS_PACKAGE_PATH=/usr/share/ros_packages
export ROS_ROOT=/usr/share/ros

. "$HOME/.alias"

. "$HOME/.functions"
