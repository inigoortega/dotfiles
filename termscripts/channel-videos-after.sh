#!/usr/bin/env sh

channel="$1"
last_watched_video="$2"

[ -n "$3" ] && dateafter="--dateafter $3"

dash $TSCRIPTS/no-response-timeout.sh -f 128 -r 10 \
    youtube-dl --get-id "$channel" $dateafter 2>/dev/null |
    while read -r line && [ "$line" != "$last_watched_video" ]; do
    echo "$line"
done

# youtube-dl --get-id "$channel" | grep -B 4 "$last_watched_video" | tee /home/initega/test/test -a &

# child_pid=$!

# echo test/test | entr -p kill $child_pid
