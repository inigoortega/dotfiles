function fish_title
    true
end

bind -M insert \ce edit_command_buffer

set fish_color_command 9ce8f8
set fish_color_error 2fc81e

. /usr/share/rosbash/rosfish
. "$HOME/.alias"

# ALIASES
# alias ll='lsd --group-dirs first --date relative -alh'
# alias lll='lsd --date relative --group-dirs first -lh'
# alias ls='lsd'
# alias sc="mksh $SCRIPTS/script-run-fzf.sh"
# alias se="mksh $SCRIPTS/script-edit-fzf.sh"
# alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'

# set PATH /usr/lib/llvm/7/bin /usr/local/sbin /usr/local/bin /usr/sbin /usr/bin /sbin /bin /opt/bin /home/initega/bin /home/initega/.local/bin /home/initega/bin /home/initega/.local/bin $PATH
