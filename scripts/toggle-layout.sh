#!/usr/bin/env sh

layout="$(setxkbmap -print -verbose 10 | grep "layout:" | awk '{print $2}')"

if [ "$layout" = "gb" ]; then
    setxkbmap es
else
    setxkbmap gb
fi
