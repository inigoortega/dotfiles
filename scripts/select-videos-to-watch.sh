#!/usr/bin/env sh

printf "%b\n" "" | xclip -i -selection clipboard

stop_file="/tmp/stop-select-videos-to-watch"
if [ ! -e $stop_file ]; then
    touch $stop_file
else
    notify-send "Error" "Already selecting"
    exit 1
fi

# Get the first one
while [ -z "$link" -a -f "$stop_file" ]
do
    sleep 0.5
    link="$(xclip -o -selection clipboard)"
    [ -n $link ] && links=$link
done

# While doesnt say to stop, continue
while [ -f $stop_file ]
do
    sleep 0.5
    link="$(xclip -o -selection clipboard)"
    if [ -n "$link" -a -z "$(printf "%b\n" "$links" | grep "$link")" ]; then
        links="$links\n$link"
    fi
    echo "$link"
done

# Get first link and download
videos_file="/tmp/selected-videos-to-watch$(date +%s)"

[ -n "$links" ] && printf "%b\n" "$links" > $videos_file

# Make mpv start playing videos from the file
[ ! -f $stop_file ] && dash $TSCRIPTS/play-videos-from-file.sh $videos_file

rm "$videos_file"
