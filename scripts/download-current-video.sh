#!/usr/bin/env sh

xdotool key ctrl+l
xdotool key ctrl+c
# xdotool type yy

# link="$(xclip -o -selection clipboard | sed -E 's|([^&]*)(&)(.*)|\1|')"
link="$(xclip -o | sed -E 's|([^&]*)(&)(.*)|\1|')"

filename="$(dash $SCRIPTS/download-selected-video.sh "$link")"

printf "%b\n" "$filename"
